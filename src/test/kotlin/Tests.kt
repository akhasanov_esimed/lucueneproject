import esimed.DataLoader
import esimed.LuceneHelper
import esimed.SearchEngine
import esimed.model.Adresse
import esimed.model.Localisation
import esimed.model.Organisme
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.lang.reflect.Executable

class Tests {
    //val triangle = Triangle(0.0,1,5)
    val lucene = LuceneHelper("./index")

    @Test
    fun dataLoader(){
        val path = "C:\\Users\\Works\\IdeaProjects\\LucueneProject\\src\\main\\resources\\organismes\\mairie-61396-01.xml"
        val organisme = DataLoader.loadOrganisme(path)
       /* Assertions.assertAll(
            Executable { Assertions.assertEquals(organisme, DataLoader.loadOrganisme(path)) }
        )
        */
    }

    fun count(){
        /*Assertions.assertAll(
            Executable { Assertions.assertEquals(53, SearchEngine(lucene).count()) }
        )*/
    }

    fun get(){
        val organisme = Organisme(id="caf-01004-01", type="caf", dateMiseAJour="2018-09-11", codeInsee="01004", nom="Caisse d'allocations familiales (Caf) de l'Ain - accueil d'Ambérieu-en-Bugey",
            source="La Direction de l'information légale et administrative (Premier ministre)",
            adresse="[Adresse(type=physique, ligne=[5 rue Berthelot]", codePostal="1500", nomCommune="Ambérieu-en-Bugey",
            localisation=""[Localisation(latitude="45.95503", longitude="5.3408143", precision="8.0")], accessibilite=null),
            Adresse(type="postale", ligne="[Caisse d'allocations familiales de l'Ain, 4 rue Aristide-Briand, CS 50314]",
                codePostal="1014", nomCommune="Bourg-en-Bresse Cedex", localisation=null, accessibilite=null)],cordonneesNum=null, ouverture=null)
    }
}