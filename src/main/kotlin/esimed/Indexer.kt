package esimed

import com.google.gson.Gson
import esimed.model.Organisme
import esimed.LuceneHelper
import esimed.model.CordonneesNum
import org.apache.lucene.document.*
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.index.Term


class Indexer(lucene: LuceneHelper) {

    private val writer: IndexWriter
    private val gson=Gson()

    init {
        val writerConfig = IndexWriterConfig(lucene.analyzer)
        writerConfig.openMode = IndexWriterConfig.OpenMode.CREATE
        //writerConfig.openMode = IndexWriterConfig.OpenMode.APPEND
        writer = IndexWriter(lucene.index, writerConfig)
    }

    fun index(organisme: Organisme) {
        val doc = Document()
        doc.add(StringField("id", organisme.id, Field.Store.YES))
        //doc.add(TextField("id", organisme.id, Field.Store.NO))
        doc.add(TextField("type", organisme.type, Field.Store.YES))
        doc.add(TextField("dateMiseAJour", organisme.dateMiseAJour, Field.Store.YES))
        doc.add(TextField("codeInsee", organisme.codeInsee, Field.Store.YES))
        doc.add(TextField("nom", organisme.nom, Field.Store.YES))
        doc.add(TextField("source", organisme.source, Field.Store.YES))
        doc.add(TextField("Adresse", gson.toJson(organisme.adresse), Field.Store.YES))
        doc.add(TextField("cordonneesNum", gson.toJson(organisme.cordonneesNum), Field.Store.YES))
        doc.add(TextField("ouverture", gson.toJson(organisme.ouverture), Field.Store.YES))
        val list = organisme.adresse
        for(adres in list!!){
            doc.add(TextField("codePostal", adres.codePostal.toString(), Field.Store.YES))
            doc.add(TextField("nomCommune", adres.nomCommune, Field.Store.NO))
            doc.add(TextField("departement", (adres.codePostal.toString()).substring(0,1), Field.Store.YES))
        }

        /*doc.add(
            TextField("startDate",
                DateTools.dateToString(filming.startDate, DateTools.Resolution.DAY),
                Field.Store.YES)
        )*/
        writer.addDocument(doc)
        commit()
        val term = Term("id", organisme.id)
        writer.updateDocument(term, doc)
    }

    fun commit() {
        writer.commit()
    }

    fun close() {
        writer.close()
    }
}