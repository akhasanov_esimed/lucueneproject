package esimed

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import esimed.model.Adresse
import esimed.model.CordonneesNum
import esimed.model.Organisme
import esimed.model.Ouverture
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.Term
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.*
import java.io.File

class SearchEngine(lucene: LuceneHelper){
    val lucene = lucene
    val indexReader: IndexReader =DirectoryReader.open(lucene.index)
    val indexSearcher: IndexSearcher=IndexSearcher(indexReader)
    private val gson = Gson()

    fun read(docid:String):Organisme {
        val term = Term("id", docid)
        val query = TermQuery(term)
        val docs = indexSearcher.search(query,1)
        val scoreDoc = indexReader.document(docs.scoreDocs[0].doc)
        val adresse:List<Adresse> =gson.fromJson(scoreDoc["Adresse"], object :TypeToken<List<Adresse>>(){}.type)
        val cordonneesNum:List<CordonneesNum> =gson.fromJson(scoreDoc["cordonneesNum"], object :TypeToken<List<CordonneesNum>>(){}.type)
        val ouverture:List<Ouverture> =gson.fromJson(scoreDoc["ouverture"], object :TypeToken<List<Ouverture>>(){}.type)
        val organisme = Organisme(scoreDoc["id"],scoreDoc["type"],scoreDoc["dateMiseAJour"],scoreDoc["codeInsee"],scoreDoc["nom"],
            scoreDoc["source"],adresse,cordonneesNum,ouverture)
        indexReader.close()
        lucene.close()
        return organisme
    }

    fun search(keywords:String, callback:(Float,Organisme) -> Unit){
        val builder = BooleanQuery.Builder()
        val term1 = Term("nom", keywords)
        val term2 = Term("codePostal", keywords)
        val query1 = TermQuery(term1)
        val query2 = TermQuery(term2)
        var nom = QueryParser("nom", lucene.analyzer).parse(keywords)
        nom = BoostQuery(nom,9.0f)
        //var type = QueryParser("type", lucene.analyzer).parse(keywords)
        //var nomCommune = QueryParser("nomCommune", lucene.analyzer).parse(keywords)
        //nomCommune = BoostQuery(nomCommune,3.0f)
        var codePostal = QueryParser("codePostal", lucene.analyzer).parse(keywords)
        //var departement = QueryParser("departement", lucene.analyzer).parse(keywords)
        //codePostal = BoostQuery(codePostal,6.0f)
        //departement = BoostQuery(departement,3.0f)
        builder.add(nom, BooleanClause.Occur.SHOULD)
        builder.add(codePostal, BooleanClause.Occur.SHOULD)
        //builder.add(codePostal, BooleanClause.Occur.MUST)
        val query = builder.build()
        val docs = indexSearcher.search(query, 10)
        println("${docs.totalHits} results founds")
        for (scoreDoc in docs.scoreDocs) {
            val doc = indexReader.document(scoreDoc.doc)
            val organisme = Organisme(doc["id"],doc["type"],doc["dateMiseAJour"],doc["codeInsee"],doc["nom"],
                doc["source"],null,null,null)
            callback(scoreDoc.score,organisme)
        }
    }

    fun get(id:String): Organisme? {
        val term = Term("id", id)
        val query = TermQuery(term)
        val docs = indexSearcher.search(query,1)
        val scoreDoc = indexReader.document(docs.scoreDocs[0].doc)
        val adresse:List<Adresse> =gson.fromJson(scoreDoc["Adresse"], object :TypeToken<List<Adresse>>(){}.type)

        /*val coodonneesNum = scoreDoc["cordonneesNum"] as List<CordonneesNum>
        val ouverture = scoreDoc["ouverture"] as List<Ouverture>
        */
        val organisme = Organisme(scoreDoc["id"],scoreDoc["type"],scoreDoc["dateMiseAJour"],scoreDoc["codeInsee"],scoreDoc["nom"],
            scoreDoc["source"],adresse,null,null)
        indexReader.close()
        lucene.close()
        return organisme
    }

    fun close() {
        indexReader.close()
    }

    fun count() : Int{
        val path = ("./index")
        return File(path).walk().count{it.isFile}
    }

}