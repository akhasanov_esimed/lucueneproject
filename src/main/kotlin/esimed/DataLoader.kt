package esimed

import esimed.model.*
import org.apache.lucene.store.Directory
import org.apache.lucene.store.NIOFSDirectory
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Path
import javax.security.sasl.AuthorizeCallback
import javax.xml.bind.JAXBContext

object DataLoader{
    fun loadOrganisme(path:String): Organisme {
        val jaxbContext = JAXBContext.newInstance(Organisme::class.java)
        val jaxbUnmarshaller = jaxbContext.createUnmarshaller()
        return jaxbUnmarshaller.unmarshal(File(path)) as Organisme
    }

    fun loadOrganismes(path:String,callback: (Organisme)->Unit) {
        val list:MutableList<Organisme> = mutableListOf()
        File(path).walk().
            forEach {
                val path:String = it.toPath().toString()
                if (it.isFile){
                    val organisme = loadOrganisme(path)
                    callback(organisme)
                }
            }
    }

}