package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Accessibilite")
data class Accessibilite(@field:XmlElement(name="type") val type:String,
                         @field:XmlElement(name="Accessibilité") val content:String){

    constructor() : this("","")

}