package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Adresse")
data class Adresse(@field:XmlAttribute(name="type") val type:String,
                   @field:XmlElement(name="Ligne")         val ligne:List<String>?,
                   @field:XmlElement(name="CodePostal")    val codePostal:Int,
                   @field:XmlElement(name="NomCommune")    val nomCommune:String,
                   @field:XmlElement(name="Localisation")  val localisation:List<Localisation>?,
                   @field:XmlElement(name="Accessibilité") val accessibilite:List<Accessibilite>?){

    constructor() : this("",null,0,"",null,null)

}