package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Localisation")
data class Localisation(@field:XmlElement(name="Latitude")val latitude:Double,
                   @field:XmlElement(name="Longitude")    val longitude:Double,
                   @field:XmlElement(name="Précision")    val precision:Double){

    constructor() : this(0.0,0.0,0.0)

}