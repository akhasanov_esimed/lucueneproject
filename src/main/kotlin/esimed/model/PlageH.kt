package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "PlageH")
data class PlageH(@field:XmlAttribute(name="début") val debut:String,
                  @field:XmlAttribute(name="fin")     val fin:String){

    constructor() : this("","")

}