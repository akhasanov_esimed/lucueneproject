package esimed.model
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Organisme")
data class Organisme(@field:XmlAttribute val id:String,
                     @field:XmlAttribute(name="pivotLocal") val type:String,
                     @field:XmlAttribute(name="dateMiseAJour") val dateMiseAJour:String,
                     @field:XmlAttribute(name="codeInsee") val codeInsee:String,
                     @field:XmlElement(name="Nom")          val nom:String,
                     @field:XmlElement(name="EditeurSource") val source:String,
                     @field:XmlElement(name="Adresse") val adresse:List<Adresse>?,
                     @field:XmlElement(name="CoordonnéesNum") val cordonneesNum:List<CordonneesNum>?,
                     @field:XmlElement(name="Ouverture")   val ouverture:List<Ouverture>?){


    constructor() : this("","","","","","",null,null,null)
}