package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "CoodonneesNum")
data class CordonneesNum(@field:XmlElement(name="Téléphone")    val telephone:String,
                       @field:XmlElement(name="Télécopie")      val telecopie:String,
                       @field:XmlElement(name="Email")        val email:String,
                       @field:XmlElement(name="Url")          val url:String){

    constructor() : this("","","","")

}