package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "PlageJ")
data class PlageJ(@field:XmlAttribute(name="début") val debut:String,
                  @field:XmlAttribute(name="fin")   val fin:String,
                  @field:XmlElement(name="PlageH")  val plageH: List<PlageH>?,
                  @field:XmlElement(name="Note")    val note:String){

    constructor() : this("","",null,"")

}