package esimed.model

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Ouverture")
data class Ouverture(@field:XmlElement(name="PlageJ") val plageJ:List<PlageJ>?){

    constructor() : this(null)

}