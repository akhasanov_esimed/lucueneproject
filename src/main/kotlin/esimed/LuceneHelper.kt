package esimed

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.fr.FrenchAnalyzer
import org.apache.lucene.store.Directory
import org.apache.lucene.store.NIOFSDirectory
import java.nio.file.FileSystems

class LuceneHelper(directory:String) {

    val index: Directory
    val analyzer: Analyzer

    init {
        val path = FileSystems.getDefault().getPath(directory)
        index = NIOFSDirectory.open(path)
        analyzer = FrenchAnalyzer()
    }

    fun close() {
        index.close()
    }
}